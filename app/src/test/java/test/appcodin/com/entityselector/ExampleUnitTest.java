package test.appcodin.com.entityselector;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void amountFormatTest(){
        BigDecimal decimal1 = new BigDecimal(35514.88386);
        String formatedValue1 = FormattingHelper.getFormattedAmount(decimal1, "TL");
        assertEquals("35.514,88 TL", formatedValue1);
        BigDecimal decimal2 = new BigDecimal(5535514.88686);
        String formatedValue2 = FormattingHelper.getFormattedAmount(decimal2, "TL");
        assertEquals("5.535.514,89 TL", formatedValue2);
    }

    @Test
    public void cardNumberFormatTest(){
        String formattedText = FormattingHelper.getFormattedCardNumber("2348664535247407");
        assertEquals("2348 **** **** 7407", formattedText);

    }

    @Test(expected = IllegalArgumentException.class)
    public void cardNumberFormatExceptionTest(){
        String formattedText = FormattingHelper.getFormattedCardNumber("2348dsa4535247407");
    }
}