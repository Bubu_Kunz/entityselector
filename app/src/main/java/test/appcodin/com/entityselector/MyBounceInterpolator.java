package test.appcodin.com.entityselector;

public class MyBounceInterpolator implements android.view.animation.Interpolator{

    MyBounceInterpolator(){

    }

    @Override
    public float getInterpolation(float time){
        float result;
        result = (float) -Math.sin(6*time);
        return result;
    }
}
