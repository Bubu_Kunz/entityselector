package test.appcodin.com.entityselector.entities;

public class Currency {
    private String isoCode;
    private Money buyingRate;
    private Money sellingRate;
    private Money minLimitForOpeningNewAccount;

    public Currency(Money buyingRate, String isoCode, Money minLimitForOpeningNewAccount, Money sellingRate) {
        this.buyingRate = buyingRate;
        this.isoCode = isoCode;
        this.minLimitForOpeningNewAccount = minLimitForOpeningNewAccount;
        this.sellingRate = sellingRate;
    }

    public Money getBuyingRate() {
        return buyingRate;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public Money getMinLimitForOpeningNewAccount() {
        return minLimitForOpeningNewAccount;
    }

    public Money getSellingRate() {
        return sellingRate;
    }
}
