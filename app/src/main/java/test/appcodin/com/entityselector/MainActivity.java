package test.appcodin.com.entityselector;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import test.appcodin.com.entityselector.entities.Entity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButton1;
    private EntitySelector mEntitySelectorAccounts;
    private EntitySelector mEntitySelectorCards;
    private Button mButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton1 = (Button) findViewById(R.id.change_state_btn);
        mButton2 = (Button) findViewById(R.id.change_state_btn2);
        mEntitySelectorAccounts = (EntitySelector) findViewById(R.id.entity_selector_accounts);
        mEntitySelectorAccounts.setEntities(MockHelper.getAccounts(10));
        mEntitySelectorCards = (EntitySelector) findViewById(R.id.entity_selector_cards);
        mEntitySelectorCards.setEntities(MockHelper.getCards(10));
        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mEntitySelectorAccounts.setVisibility(View.VISIBLE);
        mEntitySelectorCards.setVisibility(View.GONE);
        mEntitySelectorAccounts.setListener(new EntitySelector.IEventsListener() {
            @Override
            public void onChange(Entity selectedEntity) {
                Log.d("EntitySelector", "onChange");
            }

            @Override
            public void onUserInteraction() {
                Log.d("EntitySelector", "onUserInteraction");
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_state_btn:
                mEntitySelectorAccounts.setVisibility(View.VISIBLE);
                mEntitySelectorCards.setVisibility(View.GONE);
                break;
            case R.id.change_state_btn2:
                mEntitySelectorAccounts.setVisibility(View.GONE);
                mEntitySelectorCards.setVisibility(View.VISIBLE);
                break;
        }
    }
}
