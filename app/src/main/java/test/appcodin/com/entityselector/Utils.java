package test.appcodin.com.entityselector;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import test.appcodin.com.entityselector.entities.Account;
import test.appcodin.com.entityselector.entities.CreditCard;
import test.appcodin.com.entityselector.entities.Entity;

public class Utils {

    public interface IFilterCallBack{
        void filteredEntities(List<Entity> filteredEntities);
    }

    public static <T> Observable<T> toObservable(@NonNull final ObservableField<T> observableField) {
        return Observable.create(new ObservableOnSubscribe<T>() {
                                     @Override
                                     public void subscribe(@io.reactivex.annotations.NonNull final ObservableEmitter<T> e) throws Exception {
                                         if (!e.isDisposed() && observableField.get() != null) {
                                             e.onNext(observableField.get());
                                         }

                                         final android.databinding.Observable.OnPropertyChangedCallback callback = new android.databinding.Observable.OnPropertyChangedCallback() {
                                             @Override
                                             public void onPropertyChanged(android.databinding.Observable dataBindingObservable, int propertyId) {
                                                 if (dataBindingObservable == observableField && observableField.get() != null) {
                                                     e.onNext(observableField.get());
                                                 }
                                             }
                                         };

                                         observableField.addOnPropertyChangedCallback(callback);
                                     }
                                 }
        );
    }

    public static Observable<ArrayList<Entity>> listEntitiesToObservable(@NonNull final ObservableArrayList<Entity> observableField) {
       return Observable.create(new ObservableOnSubscribe<ArrayList<Entity>>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull final ObservableEmitter<ArrayList<Entity>> e) throws Exception {
                ObservableList.OnListChangedCallback<ObservableList<Entity>> callback = new ObservableList.OnListChangedCallback<ObservableList<Entity>>() {
                    @Override
                    public void onChanged(ObservableList<Entity> entities) {

                    }

                    @Override
                    public void onItemRangeChanged(ObservableList<Entity> entities, int i, int i1) {

                    }

                    @Override
                    public void onItemRangeInserted(ObservableList<Entity> entities, int i, int i1) {
                        e.onNext(new ArrayList<Entity>(entities));
                    }

                    @Override
                    public void onItemRangeMoved(ObservableList<Entity> entities, int i, int i1, int i2) {

                    }

                    @Override
                    public void onItemRangeRemoved(ObservableList<Entity> entities, int i, int i1) {

                    }
                };
                observableField.addOnListChangedCallback(callback);
            }
        });
    }

    public static void filterAsync(final String searchText, List<Entity> allEntities, final IFilterCallBack callBack){
        List<Entity> entities = new ArrayList<>();
        if(callBack != null){
            Observable.fromArray(allEntities)
                    .subscribeOn(Schedulers.computation())
                    .flatMap(new Function<List<Entity>, ObservableSource<List<Entity>>>() {
                        @Override
                        public ObservableSource<List<Entity>> apply(@io.reactivex.annotations.NonNull List<Entity> entities) throws Exception {
                            return Observable.just(filter(searchText, entities));
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<Entity>>() {
                        @Override
                        public void accept(@io.reactivex.annotations.NonNull List<Entity> entities) throws Exception {
                            callBack.filteredEntities(entities);
                        }
                    });
        }
    }


    public static List<Entity> filter(String searchText, List<Entity> allEntities){
        List<Entity> entities = new ArrayList<>();
        if(allEntities != null && allEntities.size()>0){
            if(allEntities.get(0) instanceof CreditCard){
                entities.addAll(filterCards(searchText, (List<CreditCard>)(List<?>)allEntities));
            }else if(allEntities.get(0) instanceof Account){
                entities.addAll(filterAccounts(searchText, (List<Account>)(List<?>)allEntities));
            }
            return  entities;
        }else {
            return  allEntities;
        }

    }

    private static List<CreditCard> filterCards(String searchText, List<CreditCard> allEntities){
        searchText = searchText.toLowerCase();
        List<CreditCard> result = new ArrayList<>();
        if(searchText != null && !searchText.isEmpty()){
            for(CreditCard card : allEntities){
                if(card.getCardBranch().getBranchName().toLowerCase().contains(searchText)
                        || card.getCardType().toString().toLowerCase().contains(searchText)
                        || card.getCardNumber().toLowerCase().contains(searchText)){
                    result.add(card);
                }
            }
            return result;

        }
        return allEntities;
    }

    private static List<Account> filterAccounts(String searchText, List<Account> allEntities){
        searchText = searchText.toLowerCase();
        List<Account> result = new ArrayList<>();
        if(searchText != null && !searchText.isEmpty()) {
            for(Account account : allEntities){
                if(account.getAccountBranch().getBranchName().toLowerCase().contains(searchText)
                        || account.getAccountType().toString().toLowerCase().contains(searchText)
                        || account.getIban().getNumber().toLowerCase().contains(searchText)){
                    result.add(account);
                }
            }
            return result;

        }
        return allEntities;

    }
}

