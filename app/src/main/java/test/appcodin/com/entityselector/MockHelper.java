package test.appcodin.com.entityselector;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import test.appcodin.com.entityselector.entities.Account;
import test.appcodin.com.entityselector.entities.AccountProductType;
import test.appcodin.com.entityselector.entities.AccountType;
import test.appcodin.com.entityselector.entities.Branch;
import test.appcodin.com.entityselector.entities.CardType;
import test.appcodin.com.entityselector.entities.City;
import test.appcodin.com.entityselector.entities.CreditCard;
import test.appcodin.com.entityselector.entities.Entity;
import test.appcodin.com.entityselector.entities.Iban;
import test.appcodin.com.entityselector.entities.Money;
import test.appcodin.com.entityselector.entities.SavingCode;

public class MockHelper {
    private MockHelper(){}
    private static Random mRandom = new Random();

    private static final String CURRENSCY_CODES[] = new String[]{
            "EUR",
            "GBP",
            "CNY",
            "TRY",
            "USD",
            "RUB",
            "UAH"
    };

    private static final String ACCOUNT_NAMES[] = new String[]{
            "Aaberg",
            "Aaronson",
            "Brittni",
            "Hoskinson",
            "Hourihan",
            "Phaedra",
            "Shawnee"
    };

    private static final String CREDIT_CARD_PRODUCT_NAMES[] = new String[]{
            "Dresden Bank",
            "Bank INA",
            "National Bank of Canada",
            "PNC Bank",
            "Bank SUMSEL BABEL",
            "Union Bank",
            "Idea Bank"
    };

    private static final Iban IBANS[] = new Iban[]{
            new Iban("257", "BI33 123412341234"),
            new Iban("55", "BR97 0036 0305 0000 1000 9795 493P 1"),
            new Iban("359", "BG18 RZBB 9155 0123 4567 89"),
            new Iban("506", "CR37 0126 0000 0123 4567 89"),
            new Iban("385", "HR17 2360 0001 1012 3456 5"),
            new Iban("372", "EE47 1000 0010 2014 5685"),
            new Iban("33", "FR76 3000 6000 0112 3456 7890 189")
    };

    private static final City CITIES[] = new City[]{
            new City(212, "Istanbul"),
            new City(232, "Izmir"),
            new City(312, "Ankara"),
            new City(378, "Bartin"),
            new City(248, "Burdur"),
            new City(438, "Hakkari"),
    };

    private static final Branch BRANCHES[] = new Branch[]{
            new Branch("TR", "ISTANBUL", getCity()),
            new Branch("TR", "AKALTAR/ISTANBUL", getCity()),
            new Branch("TR", "BASKENT/ANKARA", getCity()),
    };

    private static final List<AccountProductType> ACCOUNT_PRODUCT_TYPES =
            Collections.unmodifiableList(Arrays.asList(AccountProductType.values()));
    private static final List<AccountType> ACCOUNT_TYPES =
            Collections.unmodifiableList(Arrays.asList(AccountType.values()));
    private static final List<SavingCode> SAVING_CODES =
            Collections.unmodifiableList(Arrays.asList(SavingCode.values()));
    private static final List<CardType> CARD_TYPES =
            Collections.unmodifiableList(Arrays.asList(CardType.values()));

    public static Account[] getAccounts(int size){
        Account[] accounts = new Account[size];
        for(int i = 0; i<size; i++){
            accounts[i] = getAccount();
        }
        return accounts;
    }

    public static CreditCard[] getCards(int size){
        CreditCard[] creditCards = new CreditCard[size];
        for(int i = 0; i<size; i++){
            creditCards[i] = getCard();
        }
        return creditCards;
    }

    public static Account getAccount(){
        return  new Account.AccountBuilder()
                .name(getName())
                .iban(getIban())
                .accountBranch(getBranch())
                .accountProductType(getAccountProductType())
                .accountType(getAccountType())
                .accountProperty(getSavingCode())
                .balance(getMoney())
                .availableAmount(getMoney())
                .blockageAmount(getMoney())
                .hasOverDraftAccount(mRandom.nextInt(2) == 1)
                .alias(getAlias())
                .build();
    }

    public static CreditCard getCard(){
        return  new CreditCard(getMoney(), getBranch(), getCardNumber(), getCreditCardProductName(), getCardType());
    }

    private static String getCardNumber(){
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i<16; i++){
            builder.append(mRandom.nextInt(9));
        }
        return builder.toString();
    }

    private static Iban getIban(){
        return IBANS[mRandom.nextInt(IBANS.length)];
    }

    private static City getCity(){
        return CITIES[mRandom.nextInt(CITIES.length)];
    }

    private static Branch getBranch(){
        return BRANCHES[mRandom.nextInt(BRANCHES.length)];
    }

    private static String getName(){
        return ACCOUNT_NAMES[mRandom.nextInt(ACCOUNT_NAMES.length)];
    }

    private static String getCreditCardProductName(){
        return CREDIT_CARD_PRODUCT_NAMES[mRandom.nextInt(CREDIT_CARD_PRODUCT_NAMES.length)];
    }

    private static AccountProductType getAccountProductType(){
        return ACCOUNT_PRODUCT_TYPES.get(mRandom.nextInt(ACCOUNT_PRODUCT_TYPES.size()));
    }

    private static AccountType getAccountType(){
        return ACCOUNT_TYPES.get(mRandom.nextInt(ACCOUNT_TYPES.size()));
    }

    private static SavingCode getSavingCode(){
        return SAVING_CODES.get(mRandom.nextInt(SAVING_CODES.size()));
    }
    private static CardType getCardType(){
        return CARD_TYPES.get(mRandom.nextInt(CARD_TYPES.size()));
    }

    private static String getCurrencyCode(){
        return CURRENSCY_CODES[mRandom.nextInt(CURRENSCY_CODES.length)];
    }

    private static String getAlias(){
        return String.valueOf(mRandom.nextInt(100));
    }

    private static Money getMoney(){
        return new Money(new BigDecimal(mRandom.nextInt(100000)+mRandom.nextDouble()+10000), getCurrencyCode());
    }

}
