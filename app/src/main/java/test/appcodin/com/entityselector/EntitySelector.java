package test.appcodin.com.entityselector;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import test.appcodin.com.entityselector.databinding.AccountExpandedLayoutBinding;
import test.appcodin.com.entityselector.databinding.CardExpandedLayoutBinding;
import test.appcodin.com.entityselector.databinding.EntitySelectorLayoutBinding;
import test.appcodin.com.entityselector.entities.Account;
import test.appcodin.com.entityselector.entities.CreditCard;
import test.appcodin.com.entityselector.entities.Entity;

public class EntitySelector extends LinearLayout {

    private EntitySelectorViewModel mViewModel;
    private static final String TAG = EntitySelector.class.getSimpleName();
    private EntitiesAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private IEventsListener mListener;


    public interface IEventsListener{
        void onChange(Entity selectedEntity);
        void onUserInteraction();
    }


    public EntitySelector(Context context) {
        super(context);
    }


    public EntitySelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        mViewModel = getViewModelFromAttributes(context, attrs);
        View rootView = inflate(context, R.layout.entity_selector_layout, this);

        final RelativeLayout accountLayout = (RelativeLayout) rootView.findViewById(R.id.expanded_layout_account);
        final RelativeLayout cardLayout = (RelativeLayout) rootView.findViewById(R.id.expanded_layout_card);
        LinearLayout container = (LinearLayout) rootView.findViewById(R.id.main_layout);

        EntitySelectorLayoutBinding viewBinding = DataBindingUtil.bind(container);
        viewBinding.setViewModel(mViewModel);
        viewBinding.setHandlers(new ClickHandlers(mViewModel));
        CardExpandedLayoutBinding cardBinding = DataBindingUtil.bind(cardLayout);
        cardBinding.setHandlers(new ClickHandlers(mViewModel));
        AccountExpandedLayoutBinding accountBinding = DataBindingUtil.bind(accountLayout);
        accountBinding.setHandlers(new ClickHandlers(mViewModel));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.entities_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));


        mAdapter = new EntitiesAdapter();
        mAdapter.setItemClickListener(new EntitiesAdapter.ItemClickListener() {
            @Override
            public void onItemClicked(Entity entity) {
                if(mListener != null){
                    mListener.onChange(entity);
                }
                mViewModel.selectedItem.set(entity);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(null);

        viewBinding.executePendingBindings();

        Observable observableSelectedEntity = Utils.toObservable(mViewModel.selectedItem);
        observableSelectedEntity.subscribe(new Consumer<Entity>() {
            @Override
            public void accept(@NonNull Entity entity) throws Exception {
                mViewModel.isExpanded.set(false);
                mViewModel.animateBounce(entity instanceof Account ? accountLayout : cardLayout);
            }
        });
        Observable filteredEntitiesObservable2 = Utils.toObservable(mViewModel.filteredEntities);
        filteredEntitiesObservable2
                .subscribe(new Consumer<List<Entity>>() {
                    @Override
                    public void accept(@NonNull List<Entity> entities) throws Exception {
                        if(entities != null){
                            Entity[] entitiesArr = new Entity[entities.size()];
                            entitiesArr = entities.toArray(entitiesArr);
                            mAdapter.fillData(entitiesArr);
                        }
                    }
                });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(mListener != null){
            mListener.onUserInteraction();
        }
        return super.onInterceptTouchEvent(ev);
    }

    /**
     * Creates entity of EntitySelectorViewModel from custom attributes.
     *
     * @param context the context
     * @param attrs   the attributes defined in the XML file
     */
    private EntitySelectorViewModel getViewModelFromAttributes(Context context, AttributeSet attrs) {
        TypedArray attributeValuesArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EntitySelector, 0, 0);
        EntitySelectorViewModel.EntitySelectorVMBuilder builder = new EntitySelectorViewModel.EntitySelectorVMBuilder();
        try {
            builder.id(attributeValuesArray.getString(R.styleable.EntitySelector_id));
            builder.header(attributeValuesArray.getString(R.styleable.EntitySelector_header));
            builder.filteringEnabled(attributeValuesArray.getBoolean(R.styleable.EntitySelector_filteringEnabled, true));
            builder.templatesType(attributeValuesArray.getString(R.styleable.EntitySelector_templatesType));
            builder.hasInformationColumn(attributeValuesArray.getBoolean(R.styleable.EntitySelector_hasInformationColumn, true));
            builder.amountEntryConvertable(attributeValuesArray.getString(R.styleable.EntitySelector_amountEntryConvertable));
            builder.infoText(attributeValuesArray.getString(R.styleable.EntitySelector_infoText));
            builder.selectText(attributeValuesArray.getString(R.styleable.EntitySelector_selectText));
            builder.isEnabled(attributeValuesArray.getBoolean(R.styleable.EntitySelector_isEnabled, true));
        } finally {
            attributeValuesArray.recycle();
        }
        builder.selectedItem(null);
        return builder.build();
    }

    /**
     * Set @param amountEntryConvertable what is the omponent ID to link to for additional account usage.
     * @param amountEntryConvertable
     */
    public void setAmountEntryConvertable(String amountEntryConvertable) {
        mViewModel.amountEntryConvertable.set(amountEntryConvertable);
    }

    /**
     * Set @param entities array of credit cards
     * @param entities
     */
    public void setEntities(CreditCard[] entities) {
        mViewModel.entities.clear();
        mViewModel.entities.addAll(Arrays.asList(entities));
        mAdapter.fillData(entities);
    }

    /**
     * Set @param entities array of accounts
     * @param entities
     */
    public void setEntities(Account[] entities) {
        mViewModel.entities.clear();
        mViewModel.entities.addAll(Arrays.asList(entities));
        mAdapter.fillData(entities);
    }

    /**
     * Set @param filteringEnabled value what determines whether the component supports filtering. Supports True if it is true, false if it is false.
     * @param filteringEnabled
     */
    public void setFilteringEnabled(boolean filteringEnabled) {
        mViewModel.filteringEnabled.set(filteringEnabled);
    }

    /**
     * Set @param hasInformationColumn value what determines whether or not the information column exists.
     * @param hasInformationColumn
     */
    public void setHasInformationColumn(boolean hasInformationColumn) {
        mViewModel.hasInformationColumn.set(hasInformationColumn);
    }

    /**
     * Set @param header value what is the title field.
     * @param header
     */
    public void setHeader(String header) {
        mViewModel.header.set(header);
    }

    /**
     * Set @param id value what is the Component ID.
     * @param id
     */
    public void setId(String id) {
        mViewModel.id.set(id);
    }


    /**
     * Set @param infoText.
     * @param infoText
     */
    public void setInfoText(String infoText) {
        mViewModel.infoText.set(infoText);
    }

    /**
     * Set @param isEnabled value which allows the component to be active or passive.
     * @param isEnabled
     */
    public void setIsEnabled(boolean isEnabled) {
        mViewModel.isEnabled.set(isEnabled);
    }

    /**
     * Set @param listener.
     * @param listener
     */
    public void setListener(IEventsListener listener) {
        this.mListener = listener;
    }


    /**
     * Set @param searchText value what is placeholder text that shown in the search field.
     * @param searchText
     */
    public void setSearchText(String searchText) {
        mViewModel.searchText.set(searchText);
    }

    /**
     * Set @param selectedIndex.
     * @param selectedIndex
     */
    public void setSelectedIndex(Entity selectedIndex) {
       mViewModel.selectedIndex.set(selectedIndex);
    }

    /**
     * Set @param selectedItem.
     * @param selectedItem
     */
    public void setSelectedItem(Entity selectedItem) {
        mViewModel.selectedItem.set(selectedItem);
    }

    /**
     * Set @param selectText value what is the text that shown when no selection has been made.
     * @param selectText
     */
    public void setSelectText(String selectText) {
        mViewModel.selectText.set(selectText);
    }


    /**
     * Set @param templatesType.
     * @param templatesType
     */
    public void setTemplatesType(String templatesType) {
       mViewModel.setTemplatesType(templatesType);
    }


}
