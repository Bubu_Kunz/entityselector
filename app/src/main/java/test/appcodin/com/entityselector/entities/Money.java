package test.appcodin.com.entityselector.entities;

import java.math.BigDecimal;

public class Money {
    private String currency;
    private BigDecimal amount;

    public Money(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
