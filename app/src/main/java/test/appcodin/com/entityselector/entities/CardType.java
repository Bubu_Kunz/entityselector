package test.appcodin.com.entityselector.entities;

public enum CardType {
    CreditCard,
    DebitCard,
    PrepaidCard,
    All
}
