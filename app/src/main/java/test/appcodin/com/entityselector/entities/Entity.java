package test.appcodin.com.entityselector.entities;

public abstract class Entity {
    public abstract boolean equals(Object other);
    public abstract int hashCode();
}
