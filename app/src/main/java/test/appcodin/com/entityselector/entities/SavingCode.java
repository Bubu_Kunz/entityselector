package test.appcodin.com.entityselector.entities;

public enum SavingCode {
    Unknown,
    Saving,
    Corporate,
    Official,
    Other,
    Bank
}
