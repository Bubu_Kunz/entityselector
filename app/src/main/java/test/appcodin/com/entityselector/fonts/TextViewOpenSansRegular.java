package test.appcodin.com.entityselector.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewOpenSansRegular extends TextView {
    public static Typeface sTypeface;

    public TextViewOpenSansRegular(Context context) {
        super(context);
        setUpTypeFace(context);
    }

    public TextViewOpenSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpTypeFace(context);
    }

    private void setUpTypeFace(Context context) {
        if (sTypeface == null) {
            sTypeface = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        }
        this.setTypeface(sTypeface);
    }
}
