package test.appcodin.com.entityselector.entities;

public class Iban {
    private String countryCode;
    private String number;

    public Iban(String countryCode, String number) {
        this.countryCode = countryCode;
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getNumber() {
        return number;
    }
}

