package test.appcodin.com.entityselector.entities;

import java.util.Date;

public class Account extends Entity{
    private AccountType accountType;
    private String alias;
    private Branch accountBranch;
    private String number;
    private Money balance;
    private Money netBalance;
    private Money blockageAmount;
    private String name;
    private Date openingDate;
    private Date lastActivityDate;
    private Date lastBlockageDate;
    private Date lastBlockageType;
    private SavingCode accountProperty;
    private Iban iban;
    private boolean isSolidary;
    private Money availableAmount;
    private AccountProductType accountProductType;
    private int numberOfHolders;
    private boolean hasOverDraftAccount;

    public Account(AccountBuilder builder) {
        this.accountBranch = builder.accountBranch;
        this.accountProductType = builder.accountProductType;
        this.accountProperty = builder.accountProperty;
        this.accountType = builder.accountType;
        this.alias = builder.alias;
        this.availableAmount = builder.availableAmount;
        this.balance = builder.balance;
        this.blockageAmount = builder.blockageAmount;
        this.hasOverDraftAccount = builder.hasOverDraftAccount;
        this.iban = builder.iban;
        this.isSolidary = builder.isSolidary;
        this.lastActivityDate = builder.lastActivityDate;
        this.lastBlockageDate = builder.lastBlockageDate;
        this.lastBlockageType = builder.lastBlockageType;
        this.name = builder.name;
        this.netBalance = builder.netBalance;
        this.number = builder.number;
        this.numberOfHolders = builder.numberOfHolders;
        this.openingDate = builder.openingDate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public Branch getAccountBranch() {
        return accountBranch;
    }

    public AccountProductType getAccountProductType() {
        return accountProductType;
    }

    public SavingCode getAccountProperty() {
        return accountProperty;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public String getAlias() {
        return alias;
    }

    public Money getAvailableAmount() {
        return availableAmount;
    }

    public Money getBalance() {
        return balance;
    }

    public Money getBlockageAmount() {
        return blockageAmount;
    }

    public boolean isHasOverDraftAccount() {
        return hasOverDraftAccount;
    }

    public Iban getIban() {
        return iban;
    }

    public boolean isSolidary() {
        return isSolidary;
    }

    public Date getLastActivityDate() {
        return lastActivityDate;
    }

    public Date getLastBlockageDate() {
        return lastBlockageDate;
    }

    public Date getLastBlockageType() {
        return lastBlockageType;
    }

    public String getName() {
        return name;
    }

    public Money getNetBalance() {
        return netBalance;
    }

    public String getNumber() {
        return number;
    }

    public int getNumberOfHolders() {
        return numberOfHolders;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Account)) {
            return false;
        }


        Account user = (Account) o;
        if(user.name == null
                || user.number == null
                || user.availableAmount == null
                || user.accountType == null){
            return false;
        }
        boolean result = user.name.equals(name) &&
            user.number.equals(number) &&
            user.availableAmount.getAmount().equals(availableAmount.getAmount()) &&
            user.accountType.toString().equals(accountType.toString());

        return result;
    }

    @Override
    public int hashCode() {
        int result = 17;
        if(name != null){
            result = 31 * result + name.hashCode();
        }
        if(number != null){
            result = 31 * result + number.hashCode();
        }
        if(availableAmount != null){
            result = 31 * result + availableAmount.getAmount().hashCode();
        }
        if(accountType != null){
            result = 31 * result + accountType.toString().hashCode();
        }
        return result;
    }

    public static class AccountBuilder{
        private AccountType accountType;
        private String alias;
        private Branch accountBranch;
        private String number;
        private Money balance;
        private Money netBalance;
        private Money blockageAmount;
        private String name;
        private Date openingDate;
        private Date lastActivityDate;
        private Date lastBlockageDate;
        private Date lastBlockageType;
        private SavingCode accountProperty;
        private Iban iban;
        private boolean isSolidary;
        private Money availableAmount;
        private AccountProductType accountProductType;
        private int numberOfHolders;
        private boolean hasOverDraftAccount;

        public AccountBuilder() {
        }

        public AccountBuilder accountBranch(Branch accountBranch) {
            this.accountBranch = accountBranch;
            return this;
        }

        public AccountBuilder accountProductType(AccountProductType accountProductType) {
            this.accountProductType = accountProductType;
            return this;
        }

        public AccountBuilder accountProperty(SavingCode accountProperty) {
            this.accountProperty = accountProperty;
            return this;
        }

        public AccountBuilder accountType(AccountType accountType) {
            this.accountType = accountType;
            return this;
        }

        public AccountBuilder alias(String alias) {
            this.alias = alias;
            return this;
        }

        public AccountBuilder availableAmount(Money availableAmount) {
            this.availableAmount = availableAmount;
            return this;
        }

        public AccountBuilder balance(Money balance) {
            this.balance = balance;
            return this;
        }

        public AccountBuilder blockageAmount(Money blockageAmount) {
            this.blockageAmount = blockageAmount;
            return this;
        }

        public AccountBuilder hasOverDraftAccount(boolean hasOverDraftAccount) {
            this.hasOverDraftAccount = hasOverDraftAccount;
            return this;
        }

        public AccountBuilder iban(Iban iban) {
            this.iban = iban;
            return this;
        }

        public AccountBuilder solidary(boolean solidary) {
            isSolidary = solidary;
            return this;
        }

        public AccountBuilder lastActivityDate(Date lastActivityDate) {
            this.lastActivityDate = lastActivityDate;
            return this;
        }

        public AccountBuilder lastBlockageDate(Date lastBlockageDate) {
            this.lastBlockageDate = lastBlockageDate;
            return this;
        }

        public AccountBuilder lastBlockageType(Date lastBlockageType) {
            this.lastBlockageType = lastBlockageType;
            return this;
        }

        public AccountBuilder name(String name) {
            this.name = name;
            return this;
        }

        public AccountBuilder netBalance(Money netBalance) {
            this.netBalance = netBalance;
            return this;
        }

        public AccountBuilder number(String number) {
            this.number = number;
            return this;
        }

        public AccountBuilder numberOfHolders(int numberOfHolders) {
            this.numberOfHolders = numberOfHolders;
            return this;
        }

        public AccountBuilder openingDate(Date openingDate) {
            this.openingDate = openingDate;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }

}
