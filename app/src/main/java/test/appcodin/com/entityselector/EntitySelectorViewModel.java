package test.appcodin.com.entityselector;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Typeface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import test.appcodin.com.entityselector.entities.Account;
import test.appcodin.com.entityselector.entities.CreditCard;
import test.appcodin.com.entityselector.entities.Entity;
import test.appcodin.com.entityselector.entities.TemplateType;

public class EntitySelectorViewModel extends BaseObservable {

    private static final int SEARCH_ANIMATION_DURATION = 300;
    private static final String FONT_BOLD = "bold";
    private static final String FONT_MEDIUM = "medium";
    private static final String FONT_REGULAR = "regular";

    @Bindable
    public final ObservableField<String> id = new ObservableField<>();
    @Bindable
    public final ObservableField<String> header = new ObservableField<>();
    @Bindable
    public final ObservableArrayList<Entity> entities = new ObservableArrayList<>();
    @Bindable
    public final ObservableField<List<Entity>> filteredEntities = new ObservableField<>();
    @Bindable
    public final ObservableField<Entity> selectedItem = new ObservableField<>();
    @Bindable
    public final ObservableField<Entity> selectedIndex = new ObservableField<>();
    @Bindable
    public final ObservableBoolean filteringEnabled = new ObservableBoolean();

    private String templatesType;
    @Bindable
    public final ObservableBoolean hasInformationColumn = new ObservableBoolean();
    @Bindable
    public final ObservableField<String> amountEntryConvertable = new ObservableField<>();
    @Bindable
    public final ObservableField<String> infoText = new ObservableField<>();
    @Bindable
    public final ObservableField<String> selectText = new ObservableField<>();
    @Bindable
    public final ObservableField<String> searchText = new ObservableField<>();
    @Bindable
    public final ObservableBoolean isEnabled = new ObservableBoolean();
    @Bindable
    public final ObservableField<Boolean> isExpanded = new ObservableField<>();

    private boolean isBalanceVisible;

    public EntitySelectorViewModel() {
    }



    public EntitySelectorViewModel(EntitySelectorVMBuilder builder) {
        id.set(builder.id);
        header.set(builder.header);
        selectedItem.set(builder.selectedItem);
        selectedIndex.set(builder.selectedIndex);
        filteringEnabled.set(builder.filteringEnabled);
        templatesType = builder.templatesType;
        hasInformationColumn.set(builder.hasInformationColumn);
        amountEntryConvertable.set(builder.amountEntryConvertable);
        infoText.set(builder.infoText);
        selectText.set(builder.selectText);
        searchText.set(builder.searchText);
        isEnabled.set(builder.isEnabled);
        isExpanded.set(false);
        if(builder.entities != null){
            entities.addAll(Arrays.asList(builder.entities));
        }
        Observable<ArrayList<Entity>> observableEntities = Utils.listEntitiesToObservable(entities);
        observableEntities.subscribe(new Consumer<ArrayList<Entity>>() {
            @Override
            public void accept(@NonNull ArrayList<Entity> entities) throws Exception {
                if(entities != null){
                    filteredEntities.set(entities);
                    if(entities != null && searchText.get() != null){
                        Utils.filter(searchText.get(), entities);
                    }
                }
            }
        });
        Observable<Entity> observableSelectedIndex = Utils.toObservable(selectedIndex);
        observableSelectedIndex.subscribe(new Consumer<Entity>() {
            @Override
            public void accept(@NonNull Entity entity) throws Exception {
                if(entity != null){
                    selectedItem.set(entity);
                }
            }
        });
        isBalanceVisible = templatesType == null || !templatesType.equalsIgnoreCase(TemplateType.NO_BALANCE);
    }



    @BindingAdapter({"font"})
    public static void setFont(TextView textView, String fontName){
        switch (fontName){
            case FONT_BOLD:
                textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "OpenSans-Bold.ttf"));
                break;
            case FONT_REGULAR:
                textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "OpenSans-Regular.ttf"));
                break;
            case FONT_MEDIUM:
                textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "OpenSans-Semibold.ttf"));
                break;
        }
    }

    public EditText.OnFocusChangeListener getSearchFocusListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(view instanceof RelativeLayout){
                }else if(view instanceof EditText){
                    final EditText editText = (EditText) view;
                    final ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) editText.getLayoutParams();
                    if(hasFocus){
                        RelativeLayout parentLayout = (RelativeLayout) (ViewGroup) editText.getParent();

                        ValueAnimator animator = ValueAnimator.ofInt(view.getWidth(), parentLayout.getWidth());
                        animator.setTarget(editText);
                        animator.setDuration(SEARCH_ANIMATION_DURATION);
                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            public void onAnimationUpdate(ValueAnimator animation) {
                                params.width = (int) animation.getAnimatedValue();
                                editText.setLayoutParams(params);
                            }
                        });
                        animator.start();
                    }else {
                        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        editText.setLayoutParams(params);
                        searchText.set("");
                        InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }


        };
    }

    public View.OnClickListener getSearchFieldClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = (EditText)view.findViewById(R.id.edit_text_search);
                editText.setFocusableInTouchMode(true);
                editText.requestFocus();
            }
        };
    }

    public void animateBounce(final View view){
        final View bouncedView;
        final ViewGroup viewParent =   (ViewGroup) view.getParent().getParent();
        bouncedView = isExpanded.get() ?  viewParent : view;
        bouncedView.clearAnimation();
        final TranslateAnimation translation;
        Resources r = view.getResources();
        int[] location = new int[2];
        bouncedView.getLocationInWindow(location);

        final float start =  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, -10, r.getDisplayMetrics());
        final float end = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, r.getDisplayMetrics());
        translation = new TranslateAnimation(0f, 0F, start, end);
        translation.setDuration(100);
        translation.setInterpolator(new LinearInterpolator());
        bouncedView.startAnimation(translation);
        translation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if(isExpanded.get()){
                    TranslateAnimation translation2 = new TranslateAnimation(0f, 0F, start, 0);
                    translation2.setDuration(200);
                    translation2.setInterpolator(new BounceInterpolator());
                    bouncedView.startAnimation(translation2);

                }else {
                    TranslateAnimation translation2 = new TranslateAnimation(0f, 0F, 0, 5*end);
                    translation2.setDuration(200);
                    translation2.setInterpolator(new MyBounceInterpolator());
                    View container = bouncedView.findViewById(R.id.container);
                    container.startAnimation(translation2);
                }


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @BindingAdapter("android:layout_width")
    public static void setLayoutWidth(View view, float width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) width;
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_height")
    public static void setLayoutHeight(View view, float height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int) height;
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginBottom")
    public static void setBottomMargin(View view, float bottomMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                layoutParams.rightMargin, Math.round(bottomMargin));
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginTop")
    public static void setTopMargin(View view, float topMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, Math.round(topMargin),
                layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }


    public void onSearchTextChanged(final CharSequence s, int start, int before, int count) {
        searchText.set(s.toString());
        Utils.filterAsync(s.toString(), entities, new Utils.IFilterCallBack() {
            @Override
            public void filteredEntities(List<Entity> filteredEntities) {
                EntitySelectorViewModel.this.filteredEntities.set(filteredEntities);
            }
        });
    }

    @Bindable
    public boolean isBalanceVisible() {
        return isBalanceVisible;
    }

    @Bindable
    public String getTemplatesType() {
        return templatesType;
    }

    public void setTemplatesType(String templatesType) {
        this.templatesType = templatesType;
        isBalanceVisible = templatesType == null || !templatesType.equalsIgnoreCase(TemplateType.NO_BALANCE);
        notifyPropertyChanged(test.appcodin.com.entityselector.BR.templatesType);
        notifyPropertyChanged(test.appcodin.com.entityselector.BR.isBalanceVisible);
    }

    public static class EntitySelectorVMBuilder{
        private String id;
        private String header;
        private Entity[] entities;
        private Entity selectedItem;
        private Entity selectedIndex;
        private boolean filteringEnabled;
        private String templatesType;
        private boolean hasInformationColumn;
        private String amountEntryConvertable;
        private String infoText;
        private String selectText;
        private String searchText;
        private boolean isEnabled;

        public EntitySelectorVMBuilder() {
        }

        public EntitySelectorVMBuilder amountEntryConvertable(String amountEntryConvertable) {
            this.amountEntryConvertable = amountEntryConvertable;
            return this;
        }

        public EntitySelectorVMBuilder entities(Entity[] entities) {
            this.entities = entities;
            return this;
        }

        public EntitySelectorVMBuilder filteringEnabled(boolean filteringEnabled) {
            this.filteringEnabled = filteringEnabled;
            return this;
        }

        public EntitySelectorVMBuilder hasInformationColumn(boolean hasInformationColumn) {
            this.hasInformationColumn = hasInformationColumn;
            return this;
        }

        public EntitySelectorVMBuilder header(String header) {
            this.header = header;
            return this;
        }

        public EntitySelectorVMBuilder id(String id) {
            this.id = id;
            return this;
        }

        public EntitySelectorVMBuilder infoText(String infoText) {
            this.infoText = infoText;
            return this;
        }

        public EntitySelectorVMBuilder isEnabled(boolean enabled) {
            this.isEnabled = enabled;
            return this;
        }

        public EntitySelectorVMBuilder searchText(String searchText) {
            this.searchText = searchText;
            return this;
        }

        public EntitySelectorVMBuilder selectedIndex(Entity selectedItem) {
            this.selectedIndex = selectedItem;
            return this;
        }

        public EntitySelectorVMBuilder selectedItem(Entity selectedItem) {
            this.selectedItem = selectedItem;
            return this;
        }

        public EntitySelectorVMBuilder selectText(String selectText) {
            this.selectText = selectText;
            return this;
        }

        public EntitySelectorVMBuilder templatesType(String templatesType) {
            this.templatesType = templatesType;
            return this;
        }

        public EntitySelectorViewModel build() {
            return new EntitySelectorViewModel(this);
        }
    }
}
