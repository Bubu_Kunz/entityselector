package test.appcodin.com.entityselector.entities;

public enum AccountType {

    NotDefined,
    Demand,
    Fx_Demand,
    Fx_Term,
    Term,
    Credit,
    Overdraft,
    Investment


}
