package test.appcodin.com.entityselector.entities;

public enum AccountProductType {
    Unknown,
    VadesizHesap,
    VadeliHesap28VeUzeri,
    VadeliHesap28Alti,
    StandartVadeliYP,
    SabitHesap,
    DegiskenHesap,
    TurevDCD,
    TurevSwap,
    EndeksliHesap,
    BirikimliHesap,
    GecisliHesap,
    BCH,
    TurevDijital,
    Kiralikkasa,
    KonutHesabi,
    CeyizHesabi,
    TurevBariyer,
    TurevAccrual,
    MaximumTermAccount
}
