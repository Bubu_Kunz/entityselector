package test.appcodin.com.entityselector;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import test.appcodin.com.entityselector.databinding.AccountListItemLayoutBinding;
import test.appcodin.com.entityselector.databinding.CardListItemLayoutBinding;
import test.appcodin.com.entityselector.entities.Account;
import test.appcodin.com.entityselector.entities.CreditCard;
import test.appcodin.com.entityselector.entities.Entity;

public class EntitiesAdapter extends RecyclerView.Adapter<EntitiesAdapter.EntitiesViewHolder> {
    private static final String TAG = EntitiesAdapter.class.getSimpleName();
    private List<Entity> mEntities = new ArrayList<>();
    private Entity mSelectedEntity;
    private static final int VIEW_TYPE_ACCOUNT = 0;
    private static final int VIEW_TYPE_CARD = 1;
    private int mSelectedPosition = -1;
    private ItemClickListener mItemClickListener;

    public EntitiesAdapter() {
    }

    public EntitiesAdapter(Account[] entities) {
        this.mEntities.addAll(Arrays.asList(entities));
    }

    public EntitiesAdapter(CreditCard[] entities) {
        this.mEntities.addAll(Arrays.asList(entities));
    }

    public interface ItemClickListener{
        void onItemClicked(Entity entity);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public EntitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType){
            case VIEW_TYPE_CARD:
                CardListItemLayoutBinding cardsItemBinding =
                        CardListItemLayoutBinding.inflate(layoutInflater, parent, false);
                CardsViewHolder cardsViewHolder = new CardsViewHolder(cardsItemBinding);
                return cardsViewHolder;
            case VIEW_TYPE_ACCOUNT:
                AccountListItemLayoutBinding accountListItemLayoutBinding =
                        AccountListItemLayoutBinding.inflate(layoutInflater, parent, false);
                AccountsViewHolder accountsViewHolder = new AccountsViewHolder(accountListItemLayoutBinding);
                return accountsViewHolder;

            default:
                accountListItemLayoutBinding =
                        AccountListItemLayoutBinding.inflate(layoutInflater, parent, false);
                AccountsViewHolder defaultHolder = new AccountsViewHolder(accountListItemLayoutBinding);
                return defaultHolder;
        }

    }

    @Override
    public void onBindViewHolder(final EntitiesViewHolder holder, final int position) {
        final int selectedPosition = mEntities.indexOf(mSelectedEntity);
        boolean isChecked =  mSelectedEntity != null && selectedPosition == position;
        holder.bind(mEntities.get(position), isChecked, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentSelectedPosition = mEntities.indexOf(mSelectedEntity);
                if(currentSelectedPosition >= 0 && currentSelectedPosition != holder.getAdapterPosition()) {
                    notifyItemChanged(currentSelectedPosition);                }
                mSelectedEntity = mEntities.get(holder.getAdapterPosition());
                notifyItemChanged(holder.getAdapterPosition());
                if(mItemClickListener != null){
                    mItemClickListener.onItemClicked(mEntities.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mEntities != null){
            return mEntities.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(mEntities.get(0) instanceof CreditCard){
            return VIEW_TYPE_CARD;
        }else {
           return VIEW_TYPE_ACCOUNT;
        }
    }

    public void fillData(Entity[] entities){
        mEntities.clear();
        this.mEntities.addAll(Arrays.asList(entities));
        notifyDataSetChanged();
    }


    public abstract class EntitiesViewHolder<T extends Entity> extends RecyclerView.ViewHolder{

        public EntitiesViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bind(T entity, boolean isChecked, View.OnClickListener clickListener);
    }



    public class AccountsViewHolder extends EntitiesViewHolder<Account>{
        private AccountListItemLayoutBinding bind;


        public AccountsViewHolder(AccountListItemLayoutBinding bind) {
            super(bind.getRoot());
            this.bind = bind;
        }

        @Override
        public void bind(Account account, boolean isChecked, final View.OnClickListener clickListener){
            bind.setAccount(account);
            bind.setIsChecked(isChecked);
            bind.setClick(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.onClick(view);
                    }
                }
            });
        }
    }

    public class CardsViewHolder extends EntitiesViewHolder<CreditCard>{
        private CardListItemLayoutBinding binding;


        public CardsViewHolder(CardListItemLayoutBinding binding) {
           super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void bind(CreditCard creditCard, boolean isChecked, final View.OnClickListener clickListener) {
            binding.setClick(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.onClick(view);
                    }
                }
            });
            binding.setCard(creditCard);
            binding.setIsChecked(isChecked);
            binding.executePendingBindings();
        }
    }
}
