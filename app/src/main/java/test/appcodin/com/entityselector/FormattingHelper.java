package test.appcodin.com.entityselector;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import test.appcodin.com.entityselector.entities.CardType;

public class FormattingHelper {
    private FormattingHelper(){}
    final static Pattern PATTERNCARD = Pattern.compile("\\b([0-9]{4})[0-9]{0,9}([0-9]{4})\\b");

    public static String getFormattedAmount(BigDecimal amountValue, String currency){
        BigDecimal decimalPart = amountValue.setScale(2, BigDecimal.ROUND_HALF_UP).remainder(BigDecimal.ONE);
        String longValue = String.format("%,d", amountValue.longValue()).replace(',', '.');
        String decimalValue = decimalPart.toString().substring(decimalPart.toString().indexOf('.')+1, decimalPart.toString().length());
        String stringValue = longValue + "," + decimalValue + " " + currency;
        return stringValue;
    }

    public static SpannableString getFormattedAmountSpan(String amountStringValue){
        SpannableString spannableString =  new SpannableString(amountStringValue);
        spannableString.setSpan(new RelativeSizeSpan(1.5f), 0, amountStringValue.indexOf(',') + 1, 0); // set size
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, amountStringValue.length(), 0);// set color
        spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0,amountStringValue.length(), 0);
        return spannableString;
    }

    public static SpannableString formatAmount(BigDecimal amountValue, String currency){
        return getFormattedAmountSpan(getFormattedAmount(amountValue, currency));
    }

    public static String getFormattedCardNumber(String cardNumber) {
        if(cardNumber == null){
            throw new IllegalArgumentException("cardNumber can not be null");
        }
        cardNumber = cardNumber.trim();
        cardNumber = cardNumber.replace(" ","");
        if(PATTERNCARD.matcher(cardNumber).matches()){
            StringBuilder builder = new StringBuilder(cardNumber);
            builder.replace(4, 8, "****");
            builder.replace(8, 12, "****");
            builder.insert(4, " ");
            builder.insert(9, " ");
            builder.insert(14, " ");
            return builder.toString();
        }else {
            throw new IllegalArgumentException("Not correct card number format: "+cardNumber);
        }

    }

    public static String getFormattedTypeWithCurrency(CardType cardType, String currency){
        assert cardType != null;
        return cardType.toString() + " - " + currency;
    }
}
