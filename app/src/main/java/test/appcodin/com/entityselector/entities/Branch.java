package test.appcodin.com.entityselector.entities;

public class Branch {
    private String branchName;
    private String branchCode;
    private City city;

    public Branch(String branchCode, String branchName, City city) {
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.city = city;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public City getCity() {
        return city;
    }
}
