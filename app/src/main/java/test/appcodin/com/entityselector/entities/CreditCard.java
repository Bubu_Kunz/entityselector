package test.appcodin.com.entityselector.entities;

public class CreditCard extends Entity{
    private String cardNumber;
    private Branch cardBranch;
    private Money availableLimit;
    private String cardProductName;
    private CardType cardType;

    public CreditCard(Money availableLimit, Branch cardBranch, String cardNumber, String cardProductName, CardType cardType) {
        this.availableLimit = availableLimit;
        this.cardBranch = cardBranch;
        this.cardNumber = cardNumber;
        this.cardProductName = cardProductName;
        this.cardType = cardType;
    }

    public Money getAvailableLimit() {
        return availableLimit;
    }

    public Branch getCardBranch() {
        return cardBranch;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardProductName() {
        return cardProductName;
    }

    public CardType getCardType() {
        return cardType;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CreditCard)) {
            return false;
        }

        CreditCard user = (CreditCard) o;
        boolean result = user.cardNumber.equals(cardNumber) &&
                user.cardBranch.getBranchName().equals(cardBranch.getBranchName()) &&
                user.availableLimit.getAmount().equals(availableLimit.getAmount()) &&
                user.cardType.toString().equals(cardType.toString());
        return result;
    }

    @Override
    public int hashCode() {
        int result = 17;
        if(cardNumber != null){
            result = 31 * result + cardNumber.hashCode();
        }
        if(cardBranch != null && cardBranch.getBranchName() != null){
            result = 31 * result + cardBranch.getBranchName().hashCode();
        }
        if(availableLimit != null && availableLimit.getAmount() != null){
            result = 31 * result + availableLimit.getAmount().hashCode();
        }
        if(cardType != null){
            result = 31 * result + cardType.toString().hashCode();
        }
        return result;
    }
}
