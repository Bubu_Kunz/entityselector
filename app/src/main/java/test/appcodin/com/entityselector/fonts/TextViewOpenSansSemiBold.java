package test.appcodin.com.entityselector.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewOpenSansSemiBold extends TextView {
    public static Typeface sTypeface;

    public TextViewOpenSansSemiBold(Context context) {
        super(context);
        setUpTypeFace(context);
    }

    public TextViewOpenSansSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpTypeFace(context);
    }

    private void setUpTypeFace(Context context) {
        if (sTypeface == null) {
            sTypeface = Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
        }
        this.setTypeface(sTypeface);
    }
}
