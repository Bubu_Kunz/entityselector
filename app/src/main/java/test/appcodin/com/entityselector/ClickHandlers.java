package test.appcodin.com.entityselector;

import android.view.View;

public class ClickHandlers {

    private EntitySelectorViewModel viewModel;

    public ClickHandlers(EntitySelectorViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void onContainerClick(View view) {
        viewModel.isExpanded.set(!viewModel.isExpanded.get());
        viewModel.animateBounce(view);
    }

    public void onClearClick(View view) {
        viewModel.searchText.set("");
    }
}
